'use strict';

app.herdsView = kendo.observable({
    onShow: function (e) {
        app.log('herdsView.onShow: userKey = ' + app.loginModel.UserKey);
        app.hideNavButtons();
    },

    afterShow: function (e) {
        app.log('herdsView.afterShow: userKey = ' + app.loginModel.UserKey);

        app.onAddNewClick(function () {
            app.log('add herd');
        });
    }
});

(function () {
    app.herdsView.set('title', 'My Herds');
})();
