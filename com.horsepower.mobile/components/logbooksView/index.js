'use strict';

app.logbooksView = kendo.observable({
    onShow: function (e) {
        app.log('logbooksView.onShow: userKey = ' + app.loginModel.UserKey);
        app.hideNavButtons();
    },

    afterShow: function (e) {
        app.log('logbooksView.afterShow: userKey = ' + app.loginModel.UserKey);

        app.onAddNewClick(function () {
            app.log('add logbook');
        });
    }
});

(function () {
    app.logbooksView.set('title', 'Logbooks');
})();
