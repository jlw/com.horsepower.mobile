'use strict';

app.profileView = kendo.observable({
    firstName: '',
    lastName: '',
    emailAddress: '',
    phoneNumber: null,
    address: null,
    city: null,
    state: null,
    zip: null,
    country: null,

    onShow: function (e) {
        app.log('profileView.onShow: userKey = ' + app.loginModel.UserKey);
        app.hideNavButtons();
        server.getProfile(app.profileView.onGetProfile);
    },

    onGetProfile: function (data) {
        app.log('profileView.onGetProfile: data = ' + JSON.stringify(data));
        //app.profileView.set('userProfileKey', data.UserProfileKey);
        app.profileView.set('firstName', data.FirstName);
        app.profileView.set('lastName', data.LastName);
        app.profileView.set('emailAddress', data.EmailAddress);
        app.profileView.set('phoneNumber', data.PhoneNumber);
        app.profileView.set('address', data.FullAddress);
        app.profileView.set('city', data.City);
        app.profileView.set('state', data.State);
        app.profileView.set('zip', data.PostalCode);
        app.profileView.set('country', data.Country);
    },

    onSaveProfile: function (e) {
        app.log('profileView.onSaveProfile');

        var fname = this.get('firstName');
        var lname = this.get('lastName');
        var email = this.get('emailAddress');
        var phone = this.get('phoneNumber');
        var address = this.get('address');
        var city = this.get('city');
        var state = this.get('state');
        var zip = this.get('zip');
        var country = this.get('country');

        if (fname.trim() == '') {
            alert('Please enter a first name.');
            return false;
        }

        if (lname.trim() == '') {
            alert('Please enter a last name.');
            return false;
        }

        if (email.trim() == '') {
            alert('Please enter an email address.');
            return false;
        }

        var data = {
            firstName: fname,
            lastName: lname,
            emailAddress: email,
            phoneNumber: phone,
            address: address,
            city: city,
            state: state,
            zip: zip,
            country: country
        };

        var data = {
            UserKey: app.loginModel.UserKey,
            //UserProfileKey: app.profileView.userProfileKey,
            FirstName: fname,
            LastName: lname,
            EmailAddress: email,
            PhoneNumber: phone,
            FullAddress: address,
            City: city,
            State: state,
            PostalCode: zip,
            Country: country
        };

        app.log('> data = ' + JSON.stringify(data));

        server.setProfile(data, app.navigate('dashboard'));

        return false;
    }
});
