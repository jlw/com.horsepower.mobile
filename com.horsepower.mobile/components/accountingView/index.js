'use strict';

app.accountingView = kendo.observable({
    onShow: function (e) {
        app.log('accountingView.onShow: userKey = ' + app.loginModel.UserKey);
        app.hideNavButtons();
    },

    afterShow: function (e) {
        app.log('accountingView.afterShow: userKey = ' + app.loginModel.UserKey);
    }
});

(function () {
    app.accountingView.set('title', 'Accounting');
})();
