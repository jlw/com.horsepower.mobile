'use strict';

app.loginView = kendo.observable({
    username: '',
    password: '',

    onInit: function () {
        app.log('loginView.onInit: debug = ' + app.debug);

        // Adjust the header for small screens
        if (window.screen.height < 600) {
            $('#loginHeader').addClass('header-small');
        }

        // Set the position of the header labels
        $('#loginTabLabel').css({ left: $('#loginTab').position().left + 32 });
        $('#registerTabLabel').css({ left: $('#registerTab').position().left + 32 });

        // Initialize the footer buttons
        $("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
        });
    },

    onShow: function (e) {
        app.log('loginView.onShow');
        app.loginView.set('username', '');
        app.loginView.set('password', '');
    },

    afterShow: function (e) {
        app.log('loginView.afterShow');

        if (app.debug) {
            app.loginView.set('username', 'james');
            app.loginView.set('password', 'james');
        }
    },

    onLogin: function (e) {
        var usr = app.loginView.get('username').trim();
        var pwd = app.loginView.get('password').trim();

        if (usr == '') {
            alert('Please enter a user name.');
            return false;
        }

        if (pwd == '') {
            alert('Please enter a password.');
            return false;
        }

        var data = {
            username: usr,
            password: pwd
        };

        app.login(data);
    },

    onRegister: function () {
        app.navigate('register');
    },

    onForgotPassword: function () {
        app.log('onForgotPassword');
    },

    onShowFacebook: function () {
        app.log('onShowFacebook');
        var username, useremail;

        if (window.navigator.simulator === true) return;

        function onGetDataset(err, dataset) {
            app.log('onGetDataset: dataset = ' + JSON.stringify(dataset));

            if (err) {
                alert(err);
                return;
            }

            dataset.put('name', username, function (err, record) { });
            dataset.put('userkey', response.authResponse.userID, function (err, record) { });
            dataset.put('email', useremail, function (err, record) { });
            dataset.synchronize();
        };

        function onGetCredentials(err) {
            app.log('onGetCredentials: err = ' + err);

            if (err) {
                alert(err);
                return;
            }

            window.localStorage.setItem('cognitoIdentiy', AWS.config.credentials.identityId);
            var syncClient = new AWS.CognitoSyncManager();

            syncClient.openOrCreateDataset('userinfo', onGetDataset);
            app.navigate('dashboard');
        };

        function onConnect(graphData) {
            app.log('onConnect: graphData = ' + JSON.stringify(graphData));

            username = graphData.name;
            useremail = graphData.email;

            window.localStorage.setItem("cognitoIdentitySource", 'graph.facebook.com');
            window.localStorage.setItem("cognitoIdentityID", response.authResponse.accessToken);

            AWS.config.update({ region: 'us-west-2' });
            AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId: app.IdentityPoolId,
                Logins: {
                    'graph.facebook.com': response.authResponse.accessToken
                }
            });

            AWS.config.credentials.get(onGetCredentials);
        };

        function onLogin(response) {
            app.log('onLogin: response = ' + JSON.stringify(response));
            facebookConnectPlugin.api("me/?fields=email,name", ["public_profile"], onConnect, app.error);
        };

        facebookConnectPlugin.login(['email', 'public_profile'], onLogin, app.error);
    },

    onShowTwitter: function () {
        app.log('onShowTwitter');
    },

    onShowGoogle: function () {
        app.log('onShowGoogle');

        if (window.navigator.simulator === true) return;

        function onLogin(response) {
            app.log('onLogin: response = ' + JSON.stringify(response));
        };

        window.plugins.googleplus.login({}, onLogin, app.error);
        //window.plugins.googleplus.trySilentLogin({}, onLogin, app.error);
    }
});
