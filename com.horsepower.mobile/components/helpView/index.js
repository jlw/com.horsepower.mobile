'use strict';

app.helpView = kendo.observable({
    onShow: function (e) {
        app.log('helpView.onShow: userKey = ' + app.loginModel.UserKey);
        app.hideNavButtons();
    },

    afterShow: function (e) {
        app.log('helpView.afterShow: userKey = ' + app.loginModel.UserKey);
    }
});

(function () {
    app.helpView.set('title', 'Help');
})();
