'use strict';

app.registerView = kendo.observable({
    username: '',
    password: '',
    emailAddress: '',
    firstName: '',
    lastName: '',
    phoneNumber: null,
    address: null,
    city: null,
    state: null,
    zip: null,
    country: null,
    agreeToTerms: false,

    onShow: function () {
        app.log('registerView.onShow: debug = ' + app.debug);
        app.registerView.set('username', app.loginView.username);
        app.registerView.set('password', app.loginView.password);
    },

    onCompleteRegistration: function (e) {
        app.log('registerView.onCompleteRegistration');

        var usr = this.get('username');
        var pwd = this.get('password');
        var email = this.get('emailAddress');
        var fname = this.get('firstName');
        var lname = this.get('lastName');
        var phone = this.get('phoneNumber');
        var address = this.get('address');
        var city = this.get('city');
        var state = this.get('state');
        var zip = this.get('zip');
        var country = this.get('country');
        var agree = this.get('agreeToTerms');

        if (usr.trim() == '') {
            alert('Please enter a user name.');
            return false;
        }

        if (pwd.trim() == '') {
            alert('Please enter a password.');
            return false;
        }

        if (email.trim() == '') {
            alert('Please enter an email address.');
            return false;
        }

        if (fname.trim() == '') {
            alert('Please enter a first name.');
            return false;
        }

        if (lname.trim() == '') {
            alert('Please enter a last name.');
            return false;
        }

        if (agree !== true) {
            alert('You must agree to the Horse Power Terms of Use.');
            return false;
        }

        var data = {
            username: usr,
            password: pwd,
            emailAddress: email,
            firstName: fname,
            lastName: lname,
            phoneNumber: phone,
            address: address,
            city: city,
            state: state,
            zip: zip,
            country: country
        };

        app.register(data);

        return false
    },

    onCancel: function (e) {
        app.navigate('login');
    }
});
