'use strict';

app.calendarView = kendo.observable({
    selectedDate: null,

    onShow: function (e) {
        app.log('calendarView.onShow: userKey = ' + app.loginModel.UserKey);
        app.hideNavButtons();
    },

    afterShow: function (e) {
        app.log('calendarView.afterShow: userKey = ' + app.loginModel.UserKey);

        app.onAddNewClick(function () {
            app.log('add calendar');
        });
    },

    onChange: function (e) {
        app.log('calendarView.onChange: userKey = ' + app.loginModel.UserKey);
        var d = app.calendarView.get('selectedDate');
        app.log('> date = ' + kendo.toString(d, 'D'));
    }
});
