'use strict';

app.notificationsView = kendo.observable({
    onShow: function (e) {
        app.log('notificationsView.onShow: userKey = ' + app.loginModel.UserKey);
        app.hideNavButtons();
    },

    afterShow: function (e) {
        app.log('notificationsView.afterShow: userKey = ' + app.loginModel.UserKey);

        app.onAddNewClick(function () {
            app.log('add notification');
        });
    }
});

(function () {
    app.notificationsView.set('title', 'Notifications');
})();
