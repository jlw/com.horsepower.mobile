'use strict';

app.dashboardView = kendo.observable({
    dashboardKey: 0,
    userProfileKey: 0,
    isDefault: false,
    widgetData: [],
    checklistData: [],

    onShow: function (e) {
        app.log('dashboardView.onShow: userKey = ' + app.loginModel.UserKey);
        app.hideNavButtons();
        server.getDashboard(app.dashboardView.loadDashboard);
    },

    afterShow: function (e) {
        app.log('dashboardView.afterShow: userKey = ' + app.loginModel.UserKey);

        app.onSettingsClick(function () {
            app.navigate('dashboard', 'settings', 'none');
        });
    },

    loadDashboard: function (data) {
        app.log('dashboardView.loadDashboard: data = ' + JSON.stringify(data));

        var view = $('#dashboardView').data('kendoMobileView');
        view.contentElement().empty();

        app.dashboardView.dashboardKey = data.DashboardKey;
        app.dashboardView.userProfileKey = data.UserProfileKey;
        app.dashboardView.isDefault = data.IsDefault;

        data.Widgets.sort(function (x, y) { return x.Order - y.Order });

        $.each(data.Widgets, function (index, item) {
            switch (item.Type) {
                case 'Checklist':
                    $.get('/components/dashboardView/checklistWidget.html', function (html) {
                        view.contentElement().append(html);                         // add the collapsible to the view
                        kendo.init($('#dashboardChecklists'), kendo.mobile.ui);     // initialize the collapsible
                        kendo.bind($('#dashboardView'), app.dashboardView);         // initialize the grid by re-binding the view (?)
                        server.getChecklists(app.dashboardView.onGetChecklists);    // get the checklist data
                        $('#dashboardChecklistsTitle').html(item.Title);
                        if (item.Order == 1) {
                            $('#dashboardChecklists').addClass('top');
                        }
                    });
                    break;
                case 'Calendar':
                    $.get('/components/dashboardView/calendarWidget.html', function (html) {
                        view.contentElement().append(html);
                        kendo.init($('#dashboardCalendar'), kendo.mobile.ui);
                        $('#dashboardCalendarTitle').html(item.Title);
                        if (item.Order == 1) {
                            $('#dashboardCalendar').addClass('top');
                        }
                    });
                    break;
                case 'Logbook':
                    $.get('/components/dashboardView/logbookWidget.html', function (html) {
                        view.contentElement().append(html);
                        kendo.init($('#dashboardLogbook'), kendo.mobile.ui);
                        $('#dashboardLogbookTitle').html(item.Title);
                        if (item.Order == 1) {
                            $('#dashboardLogbook').addClass('top');
                        }
                    });
                    break;
                case 'News':
                    $.get('/components/dashboardView/newsWidget.html', function (html) {
                        view.contentElement().append(html);
                        kendo.init($('#dashboardNews'), kendo.mobile.ui);
                        $('#dashboardNewsTitle').html(item.Title);
                        if (item.Order == 1) {
                            $('#dashboardNews').addClass('top');
                        }
                    });
                    break;
                case 'Accounting':
                    $.get('/components/dashboardView/accountingWidget.html', function (html) {
                        view.contentElement().append(html);
                        kendo.init($('#dashboardAccounting'), kendo.mobile.ui);
                        $('#dashboardAccountingTitle').html(item.Title);
                        if (item.Order == 1) {
                            $('#dashboardAccounting').addClass('top');
                        }
                    });
                    break;
            }
        });

        app.dashboardView.widgetData = data.Widgets;
    },

    onGetChecklists: function (data) {
        app.log('dashboardView.onGetChecklists: data = ' + JSON.stringify(data));

        if (data.length == 0) {
            $('#dashboardChecklistsLabel').show();
            $('#dashboardChecklistsGrid').hide();
        }
        else {
            // Add progress data
            $.each(data, function (index, list) {
                var count = 0;
                $.each(list.ChecklistItems, function (index, item) {
                    if (item.IsComplete) count++;
                });
                list.Progress = count + '/' + list.ChecklistItems.length;
            });

            // Update the data source for the grid
            var ds = new kendo.data.DataSource({
                data: data,
                schema: {
                    model: {
                        id: 'WidgetKey',
                        fields: {
                            Title: { type: 'string' },
                            Progress: { type: 'string' },
                            ShowUntilDate: { type: 'string' },
                        }
                    }
                }
            });

            app.dashboardView.set('checklistData', ds);

            // Create the progress bars in the grid
            var grid = $('#dashboardChecklistsGrid').data('kendoGrid');
            $('#dashboardChecklistsGrid .progress:not(.k-progressbar)').each(function () {
                var row = $(this).closest('tr');
                var model = grid.dataItem(row);
                var complete = model.ChecklistItems.filter(function (item) {
                    return (item.IsComplete);
                });
                var status = complete.length + '/' + model.ChecklistItems.length;

                app.log('creating progress bars...');
                $(this).kendoProgressBar({
                    value: complete.length,
                    max: model.ChecklistItems.length
                });

                $(this).data('kendoProgressBar').progressStatus.text(status);
            });

            // Show the grid
            $('#dashboardChecklistsLabel').hide();
            $('#dashboardChecklistsGrid').show();
        }
    },
});
