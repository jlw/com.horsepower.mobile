'use strict';

/*
TO DO - Allow users to edit widget titles
*/

app.dashboardSettings = kendo.observable({
    widgetTypeData: [],
    widgetData: [],

    onInit: function (e) {
        app.log('dashboardSettings.onInit: userKey = ' + app.loginModel.UserKey);
        server.getWidgetTypes(app.dashboardSettings.loadWidgetTypeData);

        // Create the listview here (Kendo MVVM does not support the "selectable" setting)
        $('#dashboardWidgetTypeList').kendoListView({
            selectable: 'single',
            change: app.dashboardSettings.onWidgetTypeSelect,
            template: kendo.template($('#widgetTypeTemplate').html()),
        });
    },

    onShow: function (e) {
        app.log('dashboardSettings.onShow: userKey = ' + app.loginModel.UserKey);

        var ds = new kendo.data.DataSource({
            data: app.dashboardView.widgetData
        });

        app.dashboardSettings.set('widgetData', ds);

        $('#dashboardWidgetList').data('kendoListView')
    },

    loadWidgetTypeData: function (data) {
        app.log('dashboardSettings.loadWidgetTypeData: data = ' + JSON.stringify(data));
        app.dashboardSettings.widgetTypeData = [];

        var ds = new kendo.data.DataSource({
            data: data
        });

        app.dashboardSettings.set('widgetTypeData', ds);

        $('#dashboardWidgetTypeList').data('kendoListView').setDataSource(ds);
    },

    onWidgetTypeSelect: function (e) {
        app.log('dashboardSettings.onWidgetTypeSelect');

        var listView = e.sender;
        var data = listView.dataSource.view();
        var selectedElements = listView.select();

        var selected = $.map(selectedElements, function (item) {
            var index = $(item).index();
            return data[index];
        });
        app.log('> selected = ' + JSON.stringify(selected));

        data = app.dashboardSettings.widgetData.data();
        var key = data.filter(function (item) {
            return (item.WidgetTypeKey == selected[0].WidgetTypeKey);
        });

        if (key.length == 0) {
            var widget = {
                WidgetKey: -selected[0].WidgetTypeKey,
                WidgetTypeKey: selected[0].WidgetTypeKey,
                Type: selected[0].Title,
                Title: 'My ' + selected[0].Title,
                Order: 0
            };
            app.log('> new widget = ' + JSON.stringify(widget));
            app.dashboardSettings.widgetData.add(widget);
        }

        $('#addWidgetPopup').data('kendoMobileModalView').close();
    },

    onMoveWidgetDown: function (e) {
        var key = e.data.WidgetKey;
        var data = app.dashboardSettings.widgetData.data();

        var index = data.map(function (item) {
            return item.WidgetKey;
        }).indexOf(key);

        app.log('dashboardSettings.onMoveWidgetDown: key = ' + key + ', index = ' + index);

        if (index < data.length) {
            data.splice(index + 1, 0, data.splice(index, 1)[0]);
        }
    },

    onMoveWidgetUp: function (e) {
        var key = e.data.WidgetKey;
        var data = app.dashboardSettings.widgetData.data();

        var index = data.map(function (item) {
            return item.WidgetKey;
        }).indexOf(key);

        app.log('dashboardSettings.onMoveWidgetUp: key = ' + key + ', index = ' + index);

        if (index > 0) {
            data.splice(index - 1, 0, data.splice(index, 1)[0]);
        }
    },

    onDeleteWidget: function (e) {
        var key = e.button.data().key;
        var data = app.dashboardSettings.widgetData.data();
        var index = data.map(function (item) {
            return item.WidgetKey;
        }).indexOf(key);

        app.log('dashboardSettings.onDeleteWidget: key = ' + key + ', index = ' + index);

        if (index > -1) {
            data.splice(index, 1);
        }
    },

    onAddWidget: function (e) {
        app.log('dashboardView.onAddWidget');
        $('#addWidgetPopup').data('kendoMobileModalView').open();
    },

    onCancelWidget: function (e) {
        app.log('dashboardView.onCancelWidget');
        $('#addWidgetPopup').data('kendoMobileModalView').close();
    },

    onSaveSettings: function (e) {
        app.log('dashboardSettings.onSaveSettings');
        var widgets = app.dashboardSettings.widgetData.data();
        var widgetArray = [];

        $.each(widgets, function (index, item) {
            widgetArray.push({
                WidgetKey: item.WidgetKey,
                DashboardKey: item.DashboardKey,
                Type: item.Type,
                WidgetTypeKey: item.WidgetTypeKey,
                Title: item.Title,
                Order: index + 1
            });
        });

        var data = {
            DashboardKey: app.dashboardView.dashboardKey,
            UserProfileKey: app.dashboardView.userProfileKey,
            UserKey: app.loginModel.UserKey,
            IsDefault: app.dashboardView.isDefault,
            Widgets: widgetArray
        };

        app.log('> data = ' + JSON.stringify(data));

        server.setDashboard(data, function (data) {
            app.navigate('dashboard', 'view', 'none');
        });
    },

    onCancelSettings: function (e) {
        app.log('dashboardSettings.onCancelSettings');
        app.navigate('dashboard', 'view', 'none');
    },
});
