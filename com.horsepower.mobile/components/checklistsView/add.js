'use strict';

app.checklistsAdd = kendo.observable({
    checklistItemData: [],

    onInit: function (e) {
        app.log('checklistsAdd.onInit: userKey = ' + app.loginModel.UserKey);
        $('#checklistShowUntil').kendoDatePicker();

        $('#checklistAssignTo').kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            optionLabel: 'Person'
        });
    },

    onShow: function (e) {
        app.log('checklistsAdd.onShow: userKey = ' + app.loginModel.UserKey);

        app.hideNavButtons();
        app.hideDrawerButton();

        app.checklistsAdd.set('listTitle', 'My Checklist');
        app.checklistsAdd.set('showUntil', new Date());
        app.checklistsAdd.set('assignTo', null);
        app.checklistsAdd.set('checklistItemData', []);

        server.getUsers(app.checklistsAdd.onGetUsers);
    },

    onHide: function (e) {
        app.log('checklistsAdd.onHide: userKey = ' + app.loginModel.UserKey);
        app.showDrawerButton();
    },

    onGetUsers: function(data) {
        app.log('checklistsAdd.onGetUsers: data = ' + JSON.stringify(data));
        $('#checklistAssignTo').data('kendoDropDownList').setDataSource(data);
    },

    onAddChecklistItem: function (e) {
        app.log('checklistsAdd.onAddChecklistItem');
        app.checklistsAdd.set('description', '');
        $('#addItemPopup').data('kendoMobileModalView').open();
        $('#addItemPopup #checklistItemDescription').focus();
    },

    onSaveChecklist: function (e) {
        app.log('checklistsAdd.onSaveChecklist');

        if (app.checklistsAdd.assignTo == null) {
            alert('Please assign the item to a member.');
            return;
        }

        if (app.checklistsAdd.checklistItemData.length == 0) {
            alert('Please add a checklist item.');
            return;
        }

        var data = {
            UserKey: app.loginModel.UserKey,
            Title: app.checklistsAdd.listTitle,
            ShowUntilDate: app.checklistsAdd.showUntil.toLocaleDateString(),
            AssignToUserKey: app.checklistsAdd.assignTo.Value,
            ChecklistItems: app.checklistsAdd.checklistItemData.data().toJSON()  //JSON conversion is not recursive, so do it manually here
        };
        app.log('> data = ' + JSON.stringify(data));

        server.setChecklist(data, app.checklistsView.onShow);

        app.navigate('checklists', 'view', 'overlay:reverse');
        return false;
    },

    onCancelChecklist: function (e) {
        app.log('checklistsAdd.onCancelChecklist');
        app.navigate('checklists', 'view', 'overlay:reverse');
        return false;
    },

    onSaveChecklistItem: function (e) {
        app.log('checklistItemAdd.onSaveChecklistItem');

        var description = app.checklistsAdd.get('description');

        if (description.trim() != '') {
            var item = {
                Description: description
            };

            var data = app.checklistsAdd.get('checklistItemData');

            if (data.length == 0) {
                data = [];
                data.push(item);

                var dataSource = new kendo.data.DataSource({
                    data: data
                });

                app.checklistsAdd.set('checklistItemData', dataSource);
            }
            else {
                data.add(item);
            }
        }

        $('#addItemPopup').data('kendoMobileModalView').close();
        return false;
    },

    onCancelChecklistItem: function (e) {
        app.log('checklistItemAdd.onCancelChecklistItem');
        $('#addItemPopup').data('kendoMobileModalView').close();
        return false;
    },
});
