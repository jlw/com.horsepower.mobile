'use strict';

app.checklistsView = kendo.observable({
    checklistData: [],
    checklistFilterData: [],

    onInit: function (e) {
        app.log('checklistsView.onInit: userKey = ' + app.loginModel.UserKey);

        $('#checklistFilterByPerson').kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            optionLabel: 'Person',
            change: function (e) {
                //var text = this.text();
                var value = this.value();
                app.checklistsView.filterChecklists(value, '');
            }
        });

        $('#checklistFilterByHorse').kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            optionLabel: 'Horse',
            change: function (e) {
                //var text = this.text();
                var value = this.value();
                app.checklistsView.filterChecklists('', value);
            }
        });
    },

    onShow: function (e) {
        app.log('checklistsView.onShow: userKey = ' + app.loginModel.UserKey);

        app.hideNavButtons();

        server.getUsers(app.checklistsView.onGetUsers);
        server.getChecklists(app.checklistsView.onGetChecklists);
    },

    filterChecklists: function (userValue, horseValue) {
        app.log('checklistsView.filterChecklists: userValue = ' + userValue + ', horseValue = ' + horseValue);

        var data = app.checklistsView.get('checklistData').data();

        if (userValue != '') {
            var val = parseInt(userValue);
            data = data.filter(function (item) {
                return (item.AssignToUserKey === val);
            });
        }

        if (horseValue != '') {
            var val = parseInt(horseValue);
            data = data.filter(function (item) {
                return (item.HorseKey === val);
            });
        }

        if (data.length == 0) {
            data.push({ ChecklistKey: 0, Title: 'no entries' });
        }

        var dataSource = new kendo.data.DataSource({
            data: data,
            group: 'ChecklistKey'
        });

        app.checklistsView.set('checklistFilterData', dataSource);
    },

    onGetUsers: function(data) {
        app.log('checklistsView.onGetUsers: data = ' + JSON.stringify(data));
        $('#checklistFilterByPerson').data('kendoDropDownList').setDataSource(data);
    },

    onGetChecklists: function (data) {
        app.log('checklistsView.onGetChecklists: data = ' + JSON.stringify(data));

        if (data.length == 0) {
            data.push({ ChecklistKey: 0, Title: 'no entries' });
        }

        var dataSource = new kendo.data.DataSource({
            data: data,
            group: 'ChecklistKey'
        });

        app.checklistsView.set('checklistData', dataSource);
        app.checklistsView.set('checklistFilterData', dataSource);

        $('.checklistHeaderBtn').off('click').on('click', app.checklistsView.onDeleteChecklist);
        $('.checklistItemWrapper').off('click').on('click', app.checklistsView.onChecklistItemWrapperClick);
        $('.checklistItemWrapper label').off('click').on('click', app.checklistsView.onChecklistItemLabelClick);
        $('.checklistItemWrapper .km-check').off('click').on('click', app.checklistsView.onChecklistItemClick);

        app.onAddNewClick(function () {
            app.navigate('checklists', 'add', 'overlay');
        });
    },

    onDeleteChecklist: function (e) {
        var key = $(e.currentTarget).data().key;
        app.log('checklistsView.onDeleteChecklist: key = ' + key);

        if (!confirm('This checklist will be deleted.\n\nAre you sure?')) {
            return;
        }

        server.deleteChecklist(key, app.checklistsView.onShow);
    },

    onChecklistItemWrapperClick: function (e) {
        app.log('checklistsView.onChecklistItemWrapperClick');
        e.stopPropagation();
        e.preventDefault();

        var checkbox = $(e.target).children('input:checkbox');

        if (checkbox) {
            $(checkbox).click();
        }
    },

    onChecklistItemLabelClick: function (e) {
        app.log('checklistsView.onChecklistItemLabelClick');
        e.stopPropagation();
        e.preventDefault();

        var checkbox = $(e.target).parent().children('input:checkbox');

        if (checkbox) {
            $(checkbox).click();
        }
    },

    onChecklistItemClick: function (e) {
        app.log('checklistsView.onChecklistClick');
        e.stopPropagation();

        var checkbox = $(e.target)[0];
        var key = $(e.currentTarget).data().key;

        var data = {
            ChecklistKey: key,
            ChecklistItemKey: checkbox.value,
            IsComplete: checkbox.checked
        };
        app.log('> data = ' + JSON.stringify(data));

        server.setChecklistItem(data);
    }
});
