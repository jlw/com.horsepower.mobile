'use strict';

(function () {
    var app = {
        debug: true,
        loginModel: null,
        user: kendo.observable({
            fullName: ''
        })
    };

    var bootstrap = function () {
        $(function () {
            app.log('bootstrap: simulator = ' + window.navigator.simulator);
            app.log('> screen width = ' + window.screen.width + ', height = ' + window.screen.height + ', tablet = ' + (kendo.support.mobileOS && kendo.support.mobileOS.tablet));

            // Get login data
            server.getUserLogin(function (data) {
                app.log('> loginModel = ' + JSON.stringify(data.loginModel));
                if (data.loginModel.IsAuthenticated) {
                    // Log out before initializing
                    server.setLogout(function () {
                        server.getUserLogin(app.init);
                    });
                }
                else {
                    app.init(data);
                }
            });
        });
    };

    if (window.cordova) {
        document.addEventListener('deviceready', function () {
            if (navigator && navigator.splashscreen) {
                navigator.splashscreen.hide();
            }
            bootstrap();
        }, false);
    }
    else {
        bootstrap();
    }

    window.onerror = function (msg, url, lineNo, columnNo, error) {
        app.log('ERROR: msg = ' + msg + ', url = ' + url + ', line = ' + lineNo + 'column = ' + columnNo + ', error = ' + error);
        //alert(msg);
        app.logout();
        return false;
    }

    window.app = app;

    app.init = function (data) {
        app.log('init: loginModel = ' + JSON.stringify(data.loginModel));

        // Initialize the login parameters
        app.loginModel = data.loginModel;

        // Initialize the app
        app.mobileApp = new kendo.mobile.Application(document.body, {
            transition: 'slide',
            skin: 'nova',
            initial: 'components/loginView/view.html'
        });
    };

    app.keepActiveState = function _keepActiveState(item) {
        var currentItem = item;
        $('#navigation-container li.active').removeClass('active');
        currentItem.addClass('active');
    };

    app.isOnline = function () {
        if (!navigator || !navigator.connection) {
            return true;
        } else {
            return navigator.connection.type !== 'none';
        }
    };

    app.openLink = function (url) {
        if (url.substring(0, 4) === 'geo:' && device.platform === 'iOS') {
            url = 'http://maps.apple.com/?ll=' + url.substring(4, url.length);
        }

        window.open(url, '_system');
        if (window.event) {
            window.event.preventDefault && window.event.preventDefault();
            window.event.returnValue = false;
        }
    };

    app.onShowMore = function () {
        var navigation_show_more_view = $("#navigation-show-more-view");

        navigation_show_more_view.find("ul").html($("#navigation-container-more").html());

        navigation_show_more_view.find("ul a").each(function(index) {
            var icon = '<span class="km-icon km-' + $(this).data('icon') + '"></span>';
            var text = '<span class="km-text">' + $(this).text() + '</span>';

            $(this).html(icon + text).addClass('km-listview-link').attr('data-role', 'listview-link').wrap("<li></li>");
        });

        $("#more-view-back").off("click").on("click", function () {
            $("#navigation-show-more-view").hide();
        });
    };

    app.afterShowMore = function() {
        var navigation_show_more_view = $("#navigation-show-more-view");

        navigation_show_more_view.find("li").off('click touchend').on('click touchend', function() {
            navigation_show_more_view.hide();
            $('.km-tabstrip .km-state-active').removeClass('km-state-active');
        });
    };

    app.clickMore = function(e) {
        app.onShowMore();
        $("#navigation-show-more-view").show();
        app.afterShowMore();
    };

    app.navigate = function (view, page, transition) {
        app.log('navigate: view = ' + view);

        if (typeof page === 'undefined') {
            page = 'view';
        }

        if (typeof transition === 'undefined') {
            transition = app.mobileApp.options.transition;
        }

        if (view == 'login') {
            if (app.loginModel == null) {
                server.getUserLogin(function (data) {
                    app.log('> loginModel = ' + JSON.stringify(data.loginModel));
                    // Initialize the login parameters
                    app.loginModel = data.loginModel;
                    app.navigate('login', page, transition);
                });
                return;
            }
            else {
                app.log('> IsAuthenticated = ' + app.loginModel.IsAuthenticated);
                if (app.loginModel.IsAuthenticated) {
                    view = 'dashboard';
                }
            }
        }
        else if (view != 'register' &&
            (app.loginModel.UserKey == null || app.loginModel.UserKey == 0)) {
            app.logout();
            return;
        }

        var path = 'components/' + view + 'View/' + page + '.html';
        app.log('navigate: path = ' + path + ', transition = ' + transition);

        app.mobileApp.navigate(path, transition);
    };

    app.showLogin = function () {
        app.log('showLogin');
        $('#loginTab').addClass('btn-active');
        $('#registerTab').removeClass('btn-active');
        $('#loginTabLabel').show();
        $('#registerTabLabel').hide();
        $('#loginBtn').show();
        $('#registerBtn').hide();
    };

    app.showRegister = function () {
        app.log('showRegister');
        $('#loginTab').removeClass('btn-active');
        $('#registerTab').addClass('btn-active');
        $('#loginTabLabel').hide();
        $('#registerTabLabel').show();
        $('#loginBtn').hide();
        $('#registerBtn').show();
    };

    app.hideDrawer = function () {
        $('#left-drawer').data('kendoMobileDrawer').hide();
    };

    app.hideDrawerButton = function () {
        $('.btn-drawer').hide();
    }

    app.showDrawerButton = function () {
        $('.btn-drawer').show();
    }

    app.hideNavButtons = function () {
        $('.btn-add').hide();
        $('.btn-settings').hide();
    }

    app.onAddNewClick = function (callback) {
        var btn = $('.btn-add');
        btn.off('click').on('click', callback);
        btn.show();
    }

    app.onSettingsClick = function (callback) {
        var btn = $('.btn-settings');
        btn.off('click').on('click', callback);
        btn.show();
    }

    app.encryptPassword = function (pwd) {
        app.log('encryptPassword: pwd = ' + pwd);
        var encStr = '';

        if (pwd.length > 0) {
            var ek = app.loginModel.EK;
            var ef = app.loginModel.EF;
            var key = CryptoJS.enc.Utf8.parse(ek);
            var iv = CryptoJS.enc.Utf8.parse(ef);
            var encrypted = CryptoJS.AES.encrypt(pwd, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            encStr = encrypted.toString();
        }

        return encStr;
    };

    app.initUser = function (data) {
        app.log('initUser: data = ' + JSON.stringify(data));

        // Save the user key
        app.loginModel.UserKey = data.userKey;
        app.loginModel.Identity = data.identity;
        app.user.set('fullName', data.fullName);

        // Show the dashboard
        app.navigate('dashboard');
    };

    app.register = function (data) {
        app.log('register: data = ' + JSON.stringify(data));

        // Save the user data
        app.loginModel.UserName = data.username;
        app.loginModel.EmailAddress = data.emailAddress;
        app.loginModel.FirstName = data.firstName;
        app.loginModel.LastName = data.lastName;
        app.loginModel.PhoneNumber = data.phoneNumber;
        app.loginModel.FullAddress = data.address;
        app.loginModel.City = data.city;
        app.loginModel.State = data.state;
        app.loginModel.PostalCode = data.zip;
        app.loginModel.Country = data.country;

        // Encrypt the password
        var encStr = app.encryptPassword(data.password);
        app.loginModel.PasswordEdit = encStr;
        app.loginModel.Password = "G!" + encStr.substring(0, data.password.length - 2);

        // Register the user
        server.setRegistration(app.loginModel, app.initUser);
    };

    app.login = function (data) {
        app.log('login: data = ' + JSON.stringify(data));

        // Save the user name
        app.loginModel.UserName = data.username;

        // Encrypt the password
        var encStr = app.encryptPassword(data.password);
        app.loginModel.PasswordEdit = encStr;
        app.loginModel.Password = "G!" + encStr.substring(0, data.password.length - 2);

        // Log the user in
        server.setLogin(app.loginModel, app.initUser);
    };

    app.logout = function (transition) {
        app.log('logout: userKey = ' + app.loginModel.UserKey);

        if (typeof transition === 'undefined') {
            transition = app.mobileApp.options.transition;
        }
        app.log('> transition = ' + transition);

        server.setLogout(function () {
            // Clear the login data
            app.loginModel = null;

            // Clear the user data
            app.user.set('fullName', '');

            // Show the login page
            app.navigate('login', 'view', transition);
        });
    };

    app.error = function (err) {
        app.log('onError: err = ' + err);
        //$("#loginError").text(err);
        alert(err);
    };

    app.log = function (msg) {
        if (app.debug) {
            console.log(msg);
        }
    };

}());

