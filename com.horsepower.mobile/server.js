'use strict';

(function () {
    var server = {
        localUrl: 'http://localhost:50607/Mobile',
        remoteUrl: 'http://www.horsepower.com/Mobile',
    };

    window.server = server;

    server.getData = function (methodName, func) {
        var url = (app.debug ? this.localUrl : this.remoteUrl) + '/' + methodName;
        app.log('getData: url = ' + url);

        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'jsonp',
            jsonp: 'd',
            cache: false,
            async: true,
            success: function (response) {
                //log('> response = ' + JSON.stringify(response) + ' (' + url + ')');
                if (func != null) { func(response); }
            },
            error: function (err) {
                app.log('ERROR: ' + JSON.stringify(err));
            }
        });
    };

    server.postData = function (methodName, data, func) {
        var url = (app.debug ? this.localUrl : this.remoteUrl) + '/' + methodName;
        app.log('postData: url = ' + url);

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            crossDomain: true,
            dataType: 'json',
            success: function (response) {
                app.log('success: response = ' + JSON.stringify(response) + ' (' + url + ')');
                if (func != null) { func(response); }
            },
            error: function (response) {
                app.log('error: response = ' + JSON.stringify(response) + ' (' + url + ')');
                alert(response.status + ' (' + response.statusText + ')\n' + response.responseText);
            }
            //complete: function (response) {
            //    app.log('complete: response = ' + JSON.stringify(response) + ' (' + url + ')');
            //    if (response.status == 200) {
            //        if (func != null) func(response.responseText);
            //    }
            //    else {
            //        alert(response.status + ' (' + response.statusText + ')\n' + response.responseText);
            //    }
            //}
        });
    };

    server.getProfile = function (callback) {
        app.log('getProfile');

        if (app.loginModel.UserKey > 0) {
            server.getData('GetProfile?userKey=' + app.loginModel.UserKey, callback);
        }
    };

    server.getDashboard = function (callback) {
        app.log('getDashboard');

        if (app.loginModel.UserKey > 0) {
            server.getData('GetDashboard?userKey=' + app.loginModel.UserKey, callback);
        }
    };

    server.getWidgetTypes = function (callback) {
        app.log('getWidgetTypes');

        if (app.loginModel.UserKey > 0) {
            server.getData('GetWidgetTypes?userKey=' + app.loginModel.UserKey, callback);
        }
    };

    server.getChecklists = function (callback) {
        app.log('getChecklists');

        if (app.loginModel.UserKey > 0) {
            server.getData('GetChecklists?userKey=' + app.loginModel.UserKey, callback);
        }
    };

    server.getUsers = function (callback) {
        app.log('getUsers');

        if (app.loginModel.UserKey > 0) {
            server.getData('GetUserSelectList?userKey=' + app.loginModel.UserKey, callback);
        }
    };

    server.getHorses = function (callback) {
        app.log('getHorses');

        if (app.loginModel.UserKey > 0) {
            server.getData('GetHorseSelectList?userKey=' + app.loginModel.UserKey, callback);
        }
    };

    server.getUserLogin = function (callback) {
        app.log('getUserLogin');
        server.getData('GetLoginModel', callback);
    };

    server.setLogin = function (data, callback) {
        app.log('setLogin: data = ' + JSON.stringify(data));
        server.postData('Login', data, callback);
    };

    server.setLogout = function (callback) {
        app.log('setLogout');
        server.postData('Logout', null, callback);
    };

    server.setRegistration = function (data, callback) {
        app.log('setRegistration: data = ' + JSON.stringify(data));
        server.postData('Register', data, callback);
    };

    server.setProfile = function (data, callback) {
        app.log('setProfile: data = ' + JSON.stringify(data));
        server.postData('SaveProfile', data, callback);
    };

    server.setDashboard = function (data, callback) {
        app.log('setDashboard: data = ' + JSON.stringify(data));

        if (app.loginModel.UserKey > 0) {
            server.postData('SaveDashboard', data, callback);
        }
    };

    server.setChecklist = function (data, callback) {
        app.log('setChecklist: data = ' + JSON.stringify(data));

        if (app.loginModel.UserKey > 0) {
            server.postData('SaveChecklist', data, callback);
        }
    };

    server.setChecklistItem = function (data, callback) {
        app.log('setChecklistItem: data = ' + JSON.stringify(data));

        if (app.loginModel.UserKey > 0) {
            server.postData('SaveChecklistItem', data, callback);
        }
    };

    server.deleteChecklist = function (key, callback) {
        app.log('setChecklist: key = ' + key);

        var data = {
            checklistKey: key
        };

        if (app.loginModel.UserKey > 0) {
            server.postData('DeleteChecklist', data, callback);
        }
    };
}());

